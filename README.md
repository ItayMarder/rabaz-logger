## What is this?

This library is for logging with ELK + Winston.

## Install:

```
npm install --save rabaz-logger
```

## Usage:

### Code

```js
const { logger, morganLogger, rabbitLogger } = require('rabaz-logger');
// or
import { logger, morganLogger, rabbitLogger } from 'rabaz-logger';
```

### [Class: Terminal](#classesterminalmd)

### Options

-   LOGGER_ELASTIC_SEARCH_URL: url of elasticsearch - default: `http://localhost:9200`

-   LOGGER_NODE_ENV: if not production, will log also in console - default: `development`

-   LOGGER_LOG_FILE_NAME: log file name in root directory - default: `logFile.log`

-   npm_package_name: service name - default: name in package.json

### Usage

```js
// with morgan
app.use(morgan('short', { stream: morganLogger }));

// with rabbit
rabbitLogger.sent(message);
rabbitLogger.got(message);

// other cases
logger.info('info');
logger.error(err);
logger.error('Failed with error: %o', err);
```

### docker-compose.yml

```yml
version: '3.4'

volumes:
    elasticdata:
        driver: local

services:
    service-with-logs:
        image: service-image
        environment:
            LOGGER_ELASTIC_SEARCH_URL: 'http://elastic-logs:9200/'

    elastic-logs:
        image: docker.elastic.co/elasticsearch/elasticsearch:7.9.1
        environment:
            - discovery.type=single-node
            - 'ES_JAVA_OPTS=-Xms512m -Xmx512m'
        volumes:
            - elasticdata:/usr/share/elasticsearch/data
        ports:
            - 9500:9200

    kibana-logs:
        image: docker.elastic.co/kibana/kibana:7.9.1
        ports:
            - 5602:5601
        depends_on:
            - elastic-logs
        environment:
            ELASTICSEARCH_URL: http://elastic-logs:9200
            ELASTICSEARCH_HOSTS: http://elastic-logs:9200
```
