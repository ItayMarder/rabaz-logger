import * as env from 'env-var';
import './dotenv';

const config = {
    elasticSearchUrl: env.get('LOGGER_ELASTIC_SEARCH_URL').default('http://localhost:9200').asUrlString(),
    environment: env.get('LOGGER_NODE_ENV').default('development').asString(),
    logFileName: env.get('LOGGER_LOG_FILE_NAME').default('logFile.log').asString(),
    serviceName: env.get('npm_package_name').required().asString(),
};

export default config;
