import { Logger } from 'winston';
import stripAnsi from 'strip-ansi';
import createLogger from './loggerCreator';

const createMorganLogger = (logger: Logger) => {
    return {
        write(message: string) {
            logger.info(stripAnsi(message).slice(0, -1)); // remove \n added by morgan and colored logs
        },
    };
};

const createRabbitLogger = () => {
    const loggerInstance = createLogger('rabbit');
    const rabbitLogger = {
        sent: (data: any) => {
            loggerInstance.info(data, { direction: 'sent' });
        },
        got: (data: any) => {
            loggerInstance.info(data, { direction: 'got' });
        },
    };

    return rabbitLogger;
};

const loggerCreator = () => {
    const logger = createLogger();
    const morganLogger = createLogger('morgan');
    const rabbitLogger = createRabbitLogger();

    // @ts-ignore
    morganLogger.stream = createMorganLogger(morganLogger);

    return { logger, morganLogger: morganLogger.stream, rabbitLogger };
};

const loggerInstances = loggerCreator();

export const { logger } = loggerInstances;
export const { morganLogger } = loggerInstances;
export const { rabbitLogger } = loggerInstances;

export default { logger, morganLogger, rabbitLogger };
