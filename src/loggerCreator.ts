import * as winston from 'winston';
import { ElasticsearchTransport, LogData } from 'winston-elasticsearch';
import { Logger, format } from 'winston';
import config from './config';

const { serviceName } = config;

type loggerType = 'morgan' | 'rabbit' | 'default';

const consoleFormat = format.printf(({ level, message }) => {
    return `[${level}]: ${JSON.stringify(message.toString())}`;
});

const messageParser = (logMessage: any, type: loggerType) => {
    let message = '';

    if (typeof logMessage === 'string') {
        message = logMessage;
    } else if (type === 'rabbit') {
        message = logMessage.toString();
    } else {
        message = JSON.stringify(logMessage);
    }

    return message;
};

const generateField = (logData: LogData, type: loggerType) => {
    let fields = {};
    if (type === 'morgan') {
        const messageAsArray = logData.message.split(' ');
        fields = {
            ip: messageAsArray[0].slice(7),
            method: messageAsArray[2],
            url: messageAsArray[3],
            statusCode: messageAsArray[5],
            responseTime: messageAsArray[8],
        };
    }

    if (type === 'rabbit') {
        fields = {
            data: logData.message.toString(),
            direction: logData.meta.direction,
        };
    }

    return fields;
};

const elsticsearchOptions = (type: loggerType) => {
    return {
        clientOpts: { node: config.elasticSearchUrl },
        transformer: (logData: LogData) => {
            return {
                '@timestamp': new Date().toISOString(),
                service: logData.meta.serviceName,
                severity: logData.level,
                message: messageParser(logData.message, type),
                fields: generateField(logData, type),
            };
        },
    };
};

const addConsoleLogger = (logger: Logger) => {
    logger.add(
        new winston.transports.Console({
            format: winston.format.combine(winston.format.json(), winston.format.splat(), consoleFormat),
        }),
    );
};

export default (type: loggerType = 'default') => {
    const logger = winston.createLogger({
        format: winston.format.combine(winston.format.json(), winston.format.splat()),
        defaultMeta: { serviceName },
        transports: [new winston.transports.File({ filename: config.logFileName }), new ElasticsearchTransport(elsticsearchOptions(type))],
    });

    if (config.environment !== 'production') {
        addConsoleLogger(logger);
    }

    return logger;
};
